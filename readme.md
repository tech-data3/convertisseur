# Convretisseur

Converti des fichiers de donnees d'un format dans un autre:
format de fichier supporter : json, xml, sql(dumb), excel, csv

|type| json | xml | sql | excel | csv |
|:--|:--:|:--:|:--:|:--:|:--:|
| json | | &check; | &check; | &check;| &check; | &check; |
| xml | &check; | | &check; | &check; | &check; | &check; |
| sql | &check; | &check; | | &check; | &check; | &check; |
| excel | &check; | &check; | &check; | | &check; | &check; |
| csv | &check; | &check; | &check;| &check; | | &check; |

## Installation

```bash
git clone https://gitlab.com/tech-data3/convertisseur.git
cd convertisseur
./install.sh
```
## usage

```bash
convert.sh [-h] filename format
# or (but dont forget to activate the venv)
src/cli.py [-h] filename format
```

or

```python
from converter import convert
convert(filepath, format)
```

## Contexte du projet

Les formats à prendre en charge sont :

- json
- xml
- csv 
- excel 
- sql (dump sql)

On créera une fonction unique qui prend en entrée le **chemin vers le fichier** et le **format** de sortie. 
Cette fonction devra créer le fichier au bon format dans le même dossier que le fichier source et retourner 
le chemin vers le fichier créé.

```py
def convert(path: str, format: str) -> str:
    """
    path: str - chemnin vers le fichier source
    format: str - format du fichier "json", "xml", "csv", "excel", "sql"
    """
    pass
```

### Options :

Ajouter à la fonction un paramètre `split: int` qui permet de choisir le nombre de fichiers créés par la fonction 
(découper les données en différents fichiers de même taille (sauf le dernier si ca ne tombe pas juste évidement)). 
Si c’est le cas la fonction retourne une liste des chemins vers les fichiers créés.

Ajouter la possibilité de passer une liste dans `format: str | []`. 
Si c’est le cas, un (ou plusieurs si première option) fichier de chaque format est créé. Si c’est le cas la fonction 
retourne une liste des chemins vers les fichiers créés.

Ajouter la possibilité de changer les labels des colonnes en passant en paramètre un dictionnaire qui contient des paires clef-valeur du type `"acien nom de colonne": "nouveau nom de colonne"`.

Ajouter la possibilité de filtrer les données avant de produire le ou les fichiers de sortie. Passer la fonction de filtrage en paramètre.

### triche : 
[triche sqlite](https://stackoverflow.com/questions/75675/how-to-dump-the-data-of-some-sqlite3-tables)
