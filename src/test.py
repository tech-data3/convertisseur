from converter import convert

# tests
print(convert("data/data.xlsx", "sql"))
print(convert("data/data.xlsx", "xml"))
print(convert("data/data.xlsx", "json"))
print(convert("data/data.xlsx", "csv"))

print(convert("data/data.sql", "xml"))
print(convert("data/data.sql", "json"))
print(convert("data/data.sql", "csv"))
print(convert("data/data.sql", "excel"))

print(convert("data/data.xml", "sql"))
print(convert("data/data.xml", "json"))
print(convert("data/data.xml", "csv"))
print(convert("data/data.xml", "excel"))

print(convert("data/data.json", "sql"))
print(convert("data/data.json", "xml"))
print(convert("data/data.json", "csv"))
print(convert("data/data.json", "excel"))

print(convert("data/data.csv", "sql"))
print(convert("data/data.csv", "xml"))
print(convert("data/data.csv", "json"))
print(convert("data/data.csv", "excel"))
