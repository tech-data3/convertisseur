import pathlib
import sqlite3
import pandas as pd


def convert(path: str, format: str) -> str:
    """
    path: str - chemnin vers le fichier source
    format: str - format du fichier "json", "xml", "csv", "excel", "sql"
    """
    input_file = pathlib.Path(path).resolve()
    output_file = input_file.parent / (input_file.stem + "." + (format if format != 'excel' else 'xlsx'))
    if not input_file.is_file():
        raise ValueError("Cant find file")
    if format not in ["json", "xml", "csv", "excel", "sql"]:
        raise ValueError("Format not supported")
    if input_file.suffix[1:] not in ["json", "xml", "csv", "xlsx", "sql"]:
        raise ValueError("Input file extension, not supported")
    if input_file.suffix[1:] == format or input_file.suffix[1:] == 'xlsx' and format == 'excel':
        raise ValueError("Input file and output format are in the same format")

    con = cur = None

    if input_file.suffix == ".sql":
        con = sqlite3.connect(":memory:")
        cur = con.cursor()
        with open(input_file, "r") as dump_file:
            cur.executescript(dump_file.read())
        con.commit()
        reader_param = (
            f"SELECT * FROM {cur.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchall()[0][0]}",
            con
        )
    else:
        reader_param = input_file,

    if format == "sql":
        con = sqlite3.connect(":memory:")
        cur = con.cursor()
        writer_param = (
            input_file.stem,
            con
        )
    else:
        writer_param = output_file,

    df = getattr(
        pd,
        f"read_{input_file.suffix[1:] if input_file.suffix[1:] != 'xlsx' else 'excel'}",
        None
    )(*reader_param)
    getattr(
        df,
        f"to_{format}",
        None
    )(*writer_param, index=False)

    if format == "sql":
        with open(output_file, "w") as dump_file:
            for line in con.iterdump():
                dump_file.write(line + "\n")

    if cur:
        cur.close()
        con.close()

    return f"{output_file}"
