import argparse
from converter import convert

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="convert data file"
    )
    parser.add_argument('filename')
    parser.add_argument(
        'format',
        choices=["json", "xml", "csv", "excel", "sql"]
    )
    args = parser.parse_args()
    convert(args.filename, args.format)
