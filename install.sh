python3 -m venv ./.venv
source ./.venv/bin/activate
pip install -r requirements.txt
echo "$(pwd)/src" > "$(pwd)/.venv/lib/src.pth"
deactivate